param (
    [string]$BirthdayInput
)

# Versuche, das Datum zu analysieren
try {
    # Versuche, das Geburtsdatum zu parsen
    $Birthday = [datetime]::ParseExact($BirthdayInput, "dd.MM.yyyy", [System.Globalization.CultureInfo]::InvariantCulture)

    # Überprüfe, ob das eingegebene Datum in der Zukunft liegt
    if ($Birthday -gt (Get-Date)) {
        # Fehlermeldung für zukünftiges Datum
        Write-Error "Das eingegebene Datum liegt in der Zukunft. Bitte geben Sie ein Datum in der Vergangenheit oder heute ein."
    }
    else {
        # Berechne das Alter
        $Age = (Get-Date) - $Birthday

        # Berechne Jahre, Monate und Tage
        $Years = $Age.Years
        $Months = $Age.Months
        $Days = [math]::Round($Age.Days)

        # Berechne Gesamttage
        $TotalDays = [math]::Round($Age.TotalDays)

        Write-Host "Du bist $Years Jahre $Months Monate und $Days Tage alt. Das sind $TotalDays Tage insgesamt."
        
    }
}
catch {
    # Fehlermeldung für ungültiges Datumsformat
    Write-Error "Ungültiges Datumsformat. Bitte geben Sie ein Datum im Format dd.mm.yyyy an."
}
