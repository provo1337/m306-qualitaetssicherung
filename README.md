# Altersberechner

## Projektplan

### Aufgabenstellung

Der Kunde möchte ein Shell Tool, welches anhand eines mitgegebenen Alters im Format «dd.mm.jjjj». Eingabefehler und falsche Formate sollen erkannt werden. Bei Fehlern soll eine entsprechende Meldung ausgegeben werden, und der Prozess soll abgebrochen werden. Das Tool muss die Differenz zwischen dem eingegebenen Datum und dem aktuellen Datum berechnen, wobei Schaltjahre berücksichtigt werden müssen. Das Ergebnis soll in der Konsole in der Form "Das Alter ist A Jahre B Monate und C Tage, das sind D Tage" ausgegeben werden.

### Arbeitspakete und Pflichtenheft

- Tobias Steinmann -> TS
- Max Müller -> MM
- Joseph Adam -> JA

#### Arbeitspakete und Zuständigkeiten

- **Projektinitialisierung**: MM, TS (TS)
- **Anforderungsanalyse**: MM, TS, JA (TS)
- **Code-Konzept erstellen**: MM, JA (MM)
- **Programmierung Shell Tool**: 
  - Datum-Differenzberechnung
  - Schaltjahr-Berechnung
  - Error-Handling: JA, MM (JA)
- **Testing**: TS, MM (TS)
- **Dokumentierung**: TS, MM (TS)

## Zeitplan (GANTT)

![](./img/1.png)

## Code

Der Code ist in folgendem Public Repo zu finden, mit Erklärungen in den Kommentaren im Code: [Repository Link](https://gitlab.com/provo1337/m306-qualitaetssicherung)

## Ablaufdiagramm

Das Skript wird vom Benutzer geöffnet, und der Benutzer gibt einen String im gewünschten Format ein. Wenn das Format falsch ist oder das Datum ungültig ist, wird der Fehler erkannt, und der Benutzer wird aufgefordert, die Eingabe zu korrigieren. Wenn der Benutzer das Datum im richtigen Format eingegeben hat, wird der String in ein Datum umgewandelt, und die gewünschten Informationen werden berechnet. Diese Informationen werden dem Benutzer ausgegeben, und das Skript beendet sich.

![](./img/2.png)

## Testing

### Testfall 1: Gültiges Datum, Geburtstag in der Vergangenheit

- Eingabe: 15.03.1990
- Erwarteter Output: Das Alter ist 33 Jahre 6 Monate und 21 Tage, das sind 12.258 Tage
- Ausgabe: ![](./img/3.png)

### Testfall 2: Gültiges Datum, Geburtstag am aktuellen Tag

- Eingabe: 06.10.2000
- Erwarteter Output: Das Alter ist 22 Jahre 0 Monate und 0 Tage, das sind 8.031 Tage
- Ausgabe: ![](./img/4.png)

### Testfall 3: Ungültiges Datum (Falsches Format)

- Eingabe: 06-10-2000
- Erwarteter Output: Fehler: Ungültiges Datumsformat. Bitte geben Sie ein Datum im Format dd.mm.yyyy an.
- Ausgabe: ![](./img/5.png)

### Testfall 4: Ungültiges Datum (Nicht existierender Tag oder Monat)

- Eingabe: 32.01.1990
- Erwarteter Output: Fehler: Ungültiges Datum. Bitte geben Sie ein gültiges Datum an.
- Ausgabe: ![](./img/6.png)

### Testfall 5: Gültiges Datum, Geburtstag in der Zukunft

- Eingabe: 02.02.3000
- Erwarteter Output: Das eingegebene Datum liegt in der Zukunft. Bitte geben Sie ein Datum in der Vergangenheit oder heute ein.
- Ausgabe: ![](./img/7.png)

## Reviewing

In Arbeit
